package demo;

public class Demo {

	public static void main(String[] args) {
		//overload�������Foverride�����H
		Goo g = new Goo();
		Aoo b = new Boo();
		g.test(b);
	}

}

class Aoo{
	void show() {
		System.out.println("父show");
	}
}

class Boo extends Aoo {

	@Override
	void show() {
		System.out.println("子show");
	}
	
}

class Goo{
	//overload
	void test(Aoo o) {
		System.out.println("父型參數");
		o.show();
	}
	void test(Boo o) {
		System.out.println("子參");
		o.show();
	}
}